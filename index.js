const express = require('express');

const fs = require('fs');

const morgan = require('morgan');

const bodyParser = require('body-parser');

const app = express();

const router = require('./route');

const port = 5000;


app.set('views', './views');
app.set('view engine', 'ejs');

app.use(express.json());
app.use(morgan('dev'));
app.use(express.urlencoded({
    extended: false
}));


app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'css'));
app.use('/img', express.static(__dirname + 'img'));
app.use('/js', express.static(__dirname + 'js'));

app.use('/', router);

app.use(router);
app.listen(port, () => {
    console.log(`Server Listening ${port}`);
})