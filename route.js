const {
    request,
    Router
} = require('express');
const express = require('express');

const fs = require('fs');

const app = express();

const router = express.Router();


router.use(function timeLog(req, res, next) {
    console.log('Time:', Date.now());
    next();
});


// routing endpoint and handler
router.get('/', (req, res) => {
    res.render("index")
});

router.get('/maindong', (req, res) => {
    res.render("beda")
});



module.exports = router;